﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using anegamientoComposite;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
			TerrenoComponent Terreno = new TerrenoAguaTierra(0);

            int tipoTerreno;
            int contadorTerrenoPrimario = 0;

            void SolicitarDatoTerreno()
            {
                Console.WriteLine("Debe introducir el tipo de terreno visualizado (1 - Agua / 2 - Tierra / 3 - Anegado)");
                tipoTerreno = int.Parse(Console.ReadLine());
            }

            void AnalizarTerreno(TerrenoComponent terreno)
            {
                for (int x = 1; x <= 4; x++)
                {
                    SolicitarDatoTerreno();
                    if (tipoTerreno == 1)
                    {
                        TerrenoAgua T = new TerrenoAgua(x);
                        terreno.AgregarHijo(T);
                        Console.WriteLine("Se ha introducido un Terreno tipo agua con la numeración: " + T.CuartoCampo + ".\n");                
                    }
                    else if (tipoTerreno == 2)
                    {
                        TerrenoTierra T = new TerrenoTierra(x);
                        terreno.AgregarHijo(T);
                        Console.WriteLine("Se ha introducido un Terreno tipo tierra con la numeración: "+T.CuartoCampo + ".\n");
                    }
                    else if (tipoTerreno == 3)
                    {
                        TerrenoAguaTierra T = new TerrenoAguaTierra(x);
                        terreno.AgregarHijo(T);
                        Console.WriteLine("Se ha introducido un Terreno tipo anegado con la numeración: " + T.CuartoCampo+ ". Introducir datos de este cuadrante.\n");
                        AnalizarTerreno(T);
                    }
                    else
                    {
                        throw new Exception("Error en parametros de entrada");
                    }
                    contadorTerrenoPrimario += 1;
                }
            }

            while (contadorTerrenoPrimario < 4)
            {
                AnalizarTerreno(Terreno);
            }

            Console.WriteLine("El porcentaje de agua es: " + Terreno.PorcentajeAgua + ", el porcentaje de tierra es: " + Terreno.PorcentajeTierra);

            Console.ReadKey();
        }
	}
}
