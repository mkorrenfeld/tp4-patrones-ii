﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anegamientoComposite
{
    public class TerrenoAguaTierra : TerrenoComponent
    {
        public TerrenoAguaTierra(int cuadrante) : base(cuadrante)
        {
        }

        private List<TerrenoComponent> _hijos = new List<TerrenoComponent>();

        public override double PorcentajeAgua
        {
            get
            {
                var tot = default(double);
                foreach (var t in _hijos)
                    tot += t.PorcentajeAgua;
                return tot / 4d;
            }
        }

        public override double PorcentajeTierra
        {
            get
            {
                var tot = default(double);
                foreach (var t in _hijos)
                    tot += t.PorcentajeTierra;
                return tot / 4d;
            }
        }

        public override object AgregarHijo(TerrenoComponent componente)
        {
            _hijos.Add(componente);
            return null;
        }

        public override object EliminarHijo(TerrenoComponent componente)
        {
            _hijos.Remove(componente);
            return null;
        }

        public override List<TerrenoComponent> ObtenerHijos()
        {
            return _hijos;
        }
    }
}
