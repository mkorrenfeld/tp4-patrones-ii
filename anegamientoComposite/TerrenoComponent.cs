﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anegamientoComposite
{
    public abstract class TerrenoComponent
    {
        private int _cuartoCampo;
        public int CuartoCampo
        {
            get
            {
                return _cuartoCampo;
            }
        }
        public TerrenoComponent(int cuartoCampo)
        {
            _cuartoCampo = cuartoCampo;
        }
        public abstract double PorcentajeAgua { get; }
        public abstract double PorcentajeTierra { get; }
        public abstract object AgregarHijo(TerrenoComponent componente);
        public abstract object EliminarHijo(TerrenoComponent componente);
        public abstract List<TerrenoComponent> ObtenerHijos();

    }
}
