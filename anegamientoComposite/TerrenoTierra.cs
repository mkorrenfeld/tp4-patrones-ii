﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anegamientoComposite
{
    public class TerrenoTierra : TerrenoComponent
    {
        public TerrenoTierra(int cuartoCampo) : base(cuartoCampo)
        {
        }
        public override double PorcentajeAgua
        {
            get
            {
                return 0d;
            }
        }

        public override double PorcentajeTierra
        {
            get
            {
                return 100d;
            }
        }
        public override object AgregarHijo(TerrenoComponent componente)
        {
            return null;
        }

        public override object EliminarHijo(TerrenoComponent componente)
        {
            return null;
        }

        public override List<TerrenoComponent> ObtenerHijos()
        {
            return null;
        }
    }
}
